

## Vorlesungsprinzipien

- Prinzip 1: Systematisches Arbeiten (Folie Teil 1)
	- Prozessmodelle und Reifegrade
	- SCRUM und XP
- Prinzip 2: Anforderungen wirksam berücksichtigen (Folie Teil 1)
	- Requirements Engineering
	- Use-Cases
	- Spezifikationen
	- Embracing Change
- Prinzip 3: Strukturen entwerfen mit Information Hiding (Folie Teil 2)
	- UML
	- Information Hiding
	- Design Patterns
	- Kohäsion und Kopplung
- Prinzip 4: Verständlich Programmieren (Folie Teil 2)
	- Code-Konventionen
	- Kommentare/Dokumentation/Java Doc
	- Versionsmanagement
- Prinzip 5: Prüfen und Qualität hochladen (Folie Teil 2)
	- Grundlagen des Testens
	- Qualitätssicherung in agilen Methoden
- Prinzip 6: Fortschritt schätzen (Folie Teil 3)
	- Klassische Schätzverfahren
	- Schätzen in Agiler Entwicklung znd mit Burn-Down-Charts
	- Cocomo
	- Function Points
- Prinzip 7: Angemessene Planung und Management (Folie Teil 3)
	- Aufgaben der Projektleitung
	- Aspekte der Projektplanung
	- Agile Taskboards

## Wasserfallmodell nach Royce

### Allgemeine Beschreibung:

- Software systematisch, strukturiert und professionell entwickeln
- teil der traditionelle Entwicklung
- man "plätschert" immer eine Stufe abwärts
	
### Ablauf/Pfeile/Rücksprung:
	
- die sieben Grundbausteine werden nacheinander abgegangen
- es darf Rücksprünge geben, um zB Fehler im coding zu finden
- die Pfeile zeigen die Richtung, in der das Modell abgegangen wird
- großer Pfeil bedeutet, dass das System im Zweifel neu aufgesetzt werden muss

### Tests

- Am Ende wird geprüft, ob das Produkt die Anforderungen erfüllt
1. Tests finden nur ab der Implementierung von Modulen statt (Sie dienen zum Überprüfen der funktionellen Korrektheit der Software, also muss ein schon implementiertes SW-Modul vorhanden sein)
2. Tests können von einer einzigen Person durchgeführt oder sogar komplett automatisiert werden
3. Tests dauern wesentlich weniger lange und durch Automatisierung können diese noch wesentlich weniger aufwendig werden
4. bei Tests wird durch konkrete Test Cases nur die funktionelle Korrektheit der Software gestestet
5. Konkrete, messbare Ergebnisse

### Review

- Nach jedem Schritt wird das bisherige Produkt begutachtet und beurteilt
1. Reviews finden nach jeder einzelnen Phase statt
2. Bei Reviews sind mindestens 2 Personen anwesend (4-Augen-Prinzip), dadurch werden weniger Fehler übersehen
3. Reviews sind aufwendig und dauern lange (es gibt keine konkreten Test Cases wie beim testing)
4. In Reviews können funktionale, aber unter anderem auch nicht funktionate Anforderungen, der Code und sogar der Prozess selbst überprüft werden
5. Qualitativ untersuchen
		
### Vorteile:
	
- es ist gut strukturiert
- es ist immer klar, was noch getan werden muss
		
### Nachteile:
	
- Strukturierung schränkt rein -> kein zwischenzeitliches Feedback, Features werfen das Projekt zurück
		
## Ingenieursprinzipien

### Prinzipien
	
- Qualitätsbewusstsein
- Baugruppen und Wiederverwendung
- Problemlösung durch Zerlegung ("Divide et impera")
- Normen und Regeln
- Kostendenken
	
### Begründung
	
- sind Richtlinien um ein qualitatives Produkt Zeit- und Kosteneffizient
- die beidseitige Zufriedenheit Qualität/Kosten
- Software wird immer größer/komplexer (Wiederverwendung)(Normen und Regeln)
		
## Software
	
### Begriffserklärung
	
- "Software ist Computerprogramme, ihre Prozeduren, sowie mögliche Dokumentation und Daten betreffend der Operation eines Systems"
- "Computer programs, procedures and possibly associated documentation and data pertaining to the operation of a computer system"
		
## Software Engineering (SE)

### Begriffserklärung
	
- "Anwendung eines systematischen, disziplinierten und quantifizierbaren Ansatz der Entwicklung, Operation und Instanthaltung von Software; die Anwendung von Ingenieursprinzipien auf Software"
- "the application of a systematic, disciplined, quantifiable approach to the development, operation, and maintenance of software; that is, that is, the application of engineering to software"
		
## V-Modell

### Tests
	
- bei der Software wird auf Funktionalität geprüft
- passiert ganz am Ende der Entwicklungsphase
	
### Review
	
- was hat geklappt/was hat nicht geklappt?
- "4-Augen-Prinzip wird angewendet"
- passiert während der Entwicklungsphase
		
### Unterschiede: Review und Test
	
- Tests: formell, Unittest
- Review: informell
		
## Capability Maturity Model (CMM)

![](https://gitlab.com/cediwelli/lernen/-/raw/main/cmm.png)

1. inital
2. wiederholbar
3. definiert
4. gehandhabt
5. optimierend
	
### Beschreibung
	
- CMM bewertet den Reifegrad eines Unternehmens
- stellt kein Prozessmodell dar

## Agile Entwicklung

### Einfacher Entwurf (Folie Teil 2 Seit 30)

- Unnötige Mehrarbeit vermeiden
- YAGNI -> Nicht auf Vorrat arbeiten
- Einfacher Entwurf bedeutet:
	- Einfach zu implementieren
	- Einfach zu ändern
	- Einfach zu testen
	- Einfach zu verstehen 
	
### 'Agile Manifesto'

![](https://gitlab.com/cediwelli/lernen/-/raw/main/agilemanifesto.png)

1. "Responding to change over following a plan"		
	- Anpassung wichtiger als striktem Plan folgen
	- es muss
		- flexibler gearbeitet werden
		- kürzere Arbeitsintervalle geben
		- der Kunde mit eingebunden werden 
		- häufige Prüfungen geben

### Agile Prinzipien

- Höchste Priorität: Kundenzufriedenheit
	- durch frühe und häufige Auslieferung laufender Software
	- Enge Kundeneinbindung, Planungssitzung
- Einfachheit ist wichtigstes Konstruktionsprinzip
	- Möglichst wenige Arbeit
- Auch späte Änderung werden willkommen geheißen

			
### Agiler Zyklus (Folie Teil 1 S.129) 

- Beeinhaltet Planning Game

#### Ablauf
- "Der Kunde entwickelt vage Anforderungen, die von dem Onsite-Customer als niedergeschriebene Story Cards an die Entwickler übergeben werden. Die Entwickler machen eine Schätzung dieser, welche sie in der Zeit schaffen, vom Aufwand her gesehen, und geben diese Schätzung an den Onsite-Customer zurück, der dann auswählen soll, welche Story Cards bearbeitet werden sollen. Dann werden diese implementiert, wobei Code entsteht. Ausserdem wird gleichzeitig Dokumentation geschrieben. Danach wird ein fertiges Inkrement erzeugt, welches dann vom Kunden oder vom Onsite-Customer benutzt werden kann. Der Kunde kann dem Onsite.Customer dann Feedback geben, damit der Onsite-Customer in der nächsten Runde besser weis, was wichtig ist."

![](https://gitlab.com/cediwelli/lernen/-/raw/main/feedbackschleife.png)

## Story Cards (Folie Teil 1 S.130)

- Vom Kunden geschriebene Anforderugen
- Bewertet mit relativen Aufwandspunkten vom Entwickler
- Auf ihrem Stapel sortiert nach Wichtigkeit

## Lastenheft und Pflichtenheft

### Lastenheft
	
- User Specification
- gibt Anforderungen an die Software vor
- wird aus Anwendersicht geschrieben
- kommt vom Kunden
		
### Pflichtenheft
	
- System Specification
- gibt Design und Architektur wieder
- Antwort auf das Lastenheft
- wird vom Auftragnehmer erstellt
		
### Beispiel: 
	
#### Szenario: Dateispeicherung bei Microsoft Office Word
		
- Lastenheft: Es muss es dem Benutzer ermöglicht werden, das geschriebene Dokument an einem beliebigen Ort zu speichern
- Pflichtenheft: Die Software muss über einen Button in der grafischen Oberfläche verfügen, die es dem Anwender erlaubt, das geschriebene Dokument auf die Festplatte zu schreiben
			
## Inkrementelle Entwicklung vs Prototyping

### Inkrementelle Entwicklung
	
- in dauerhaften und gleichgroßen Zeitschritten an der Software weiterarbeiten.
- nach jedem Inkrement gibt es ein funktionierendes Programm
- Entwicklung in Bausteinen (Inkrementen)
	- Grobe Visionen (Master Plan)
	- Spätere Inkremente nicht detaiiliert
	- Jeweils ein Inkrement genauer
	- kurze Inkremente zusammengesetzt
- Versetzte Entwicklungsstränge
	- Jedes Inkrement für sich vollwertig einsetzbar
	- Relativ Unabhängig während der Entwicklung
	- Timeboxing (Immer im selben Zeittakt)

### Kurze Release-Zyklen

- Ziel: Häufiges Feeback durch den Kunden
- Operative Nutzung der Elemente
		
## Prototyping
	
- nur ein kleiner Ausschnitt des zu entwickelnden Gesamtprogramms
- Zurschaustellung einzelner Features
- für das eigentliche Projekt lernen
		
## SCRUM

### Grundidee
	
- Agile Entwicklungsmethode
- kurze Intervalle (Sprints)
- Product Backlog
- Sprint Backlog
- viel Kommunikation -> Daily Scrum
- hohe Kundenzufriedenheit erzielen
		
### Backlog
	
- der Product Backlog besteht aus User Stories
- der Product Backlog wird vom Product Owner organisiert
- der Sprint Backlog ist es der obere Teil des Product Backlogs, der für einen Sprint verwendet wird
- Product und Sprint Backlog sind für den Kunden nach Wichtigkeit sortiert
		
### Sprint
	
- immer gleichgroße Zeiteinheit (30 Tage), in der das Team ungestört an ihren AUfgaben arbeitet
- Teil der Entwicklungsphase
		
### Inkrement
	
- ein nutzbares Produkt am Ende eines Sprints, dass vom Kunden bereits verwendet werden könnte
		
### Rollen
	
1. Product Owner
			
	- Repräsentant vom Kunden oder Kunde selbst
	- Organisiert den Product Backlog
	- kann spontane Veränderungen veranlassen
			
2. SCRUM Master
		
	- verteidigt das Team nach außen.
	- NICHT der Projektleiter
	- organisiert den Sprint Backlog
	- moderiert das Daily Scrum
			
3. SCRUM Team
		
	- Die Programmierer
	- arbeiten an der Software innerhalb des Sprints
	- beeinhaltet 7 +/- 2 Entwickler oder Entwicklerpaare
	- müssen an den Daily Scrums teilnehmen

4. Coach

### Fortschrittskontrolle

- Ähnlich wie Burndown Chard mit geschätztem Aufwand der User Stories auf dem Sprint Backlog
- Aufwand aller ausstehenden User Stories verfolgen
- Pragmatischer Ansatz:
	- Anzahl der User Stories verfolgen
	- Einfacher und nicht viel ungenauer
			
## eXtreme Programing (XP)

- Kent Back

### YAGNI 
- "You ain't gonna need it"
- keine Features einbauen die (noch) nicht geplannt sind
- das System nicht so gestalten, dass es flexibel ist, obwohl es garnicht notwendig ist
- ist wichtig, weil:
	- "mit leichtem Gepäck reisen"
	- Programmieraufwand sparen
	- Einfachheit anstreben

### One-Site Customer (Folie Teil 1 S.137)

- Den Kunde Vorort vertretend
- Muss nicht immer Vorort sein, aber schnell erreichbar
- Auch "Customer Proxy" falls es sich nicht um einen echten Kunden handelt

![](https://gitlab.com/cediwelli/lernen/-/raw/main/XP.png)
		
## Symmetry of Ignorance

- Beschreibt gegenseitige Unwissenheit zwischen Entwicklern und Kunde
- Kunde weis nicht, was Software kann
- Entwickler kennt sich nicht mit dem Fach des Kunden aus
- Führt zu Missverständnissen und Frustration
	
## Requirements Engineering (RE) (Folie Teil 1 S.78)
	
- Klassen/Typen von Anforderungen (Folie Teil 1 S.95)

![](https://gitlab.com/cediwelli/lernen/-/raw/main/requirementsengineering.png)
![](https://gitlab.com/cediwelli/lernen/-/raw/main/anforderungen.png)
![](https://gitlab.com/cediwelli/lernen/-/raw/main/requirements.png)


### Elicitation
	
- Anforderungen finden ("herauskitzeln")
- Ablauf: 
	1. Stakeholder herausfinden (wer ist betroffen)
	2. Umfeld des Systems erheben (Altsysteme, Schnittstellen, Dokumentation)
	3. Sprechen mit Stakeholdern
		- Interviews/Workshops/Fragebögen
		- Beobachtung und Aufzeichnung+
- Rohanforderungen müssen danach veredelt werden
	
### Interpretation
	
- Zusprechen von Bedeutung
- "Spreu vom Weizen trennen"
	- was wird benötigt, was ist Nebensache
	- was ist wirklich gefordert, was nur Erläuterungen
- Ordnen und Sortieren (ähnliche Anforderungen zusammen)
- Detaillieren und Konkretisieren
	- Prüfbar machen
	- Formulierung schärfen
	
### Negotiation
	
1. Abhängigkeiten definieren
	- Anforderungen widersprechen sich
	- Eine Anforderung erzwingt eine andere
2. Widersprüchliche Anforderungen identifizieren
3. Verhandlungsprozess mit Moderation
	- Interessen aller diskutieren
4. Inkonsistenzen auflösen
	
### Dokumentation
	
- Anforderungen müssen fixiert werden
- Anforderungen dürfen sich ändern
- alles dokumentieren
	- Gesprächspartner, Rollen, Ziele
	- Hintergründe und Randbedingungen
	- Annahmen
	- Absichten, Rivalitäten, alte Fehlversuche 
				
### Validation
	
- Fragestellung: Sind die dokumentierten Anforderungen auch die richtigen Anforderungen
- Prüfungstechniken
	- Befragung
	- Interview
	- Nachprüfung
	- Konfrontation
	
### Verification

- Fragestellung: Stimmen aufgeschriebene Anforderungen mit den zuvor dokumentierten überein?
- Prüfungstechniken:
	- Formale Verfahren
	- Konsistenzprüfungen
	- Erreichbarkeit
	- 
## Requirements Management (Folie Teil 1 S.102)

- Verwaltung der Anforderungen
- Leicht handhabbar ablegen (Werkzeuge für RE)
- Änderungen und Versionen im Griff behalten (Änderungsmanagement)
- Änderungen weitergeben, Verfolgbarkeit in beide Richtungen (Tracing)

## Refactoring (Folie Teil 2 S.38)

- Vereinfachung von Programmen ohne Änderungen des Verhaltens
	- entfernen von Code Smells
	- garantiert durch Testen
- Vorgehen nach fest definierten Umbauplänen (Martin Fowler)
- 

### Code Smells (Folie Teil 2 S.37)

- Zu lange Parameterlisten
- Klassen mit zuvielen Funktionen
- Klassen mit zu wenigen Methoden
- Verworrene oder zu lange Methoden
- Codedopplung/Redundanz
- zu allgemeiner Code mit Spezialfällen

### Kohäsion und Kopplung

- Kohäsion ist der innere Zusammenhalt von Klassen - sollte möglichst hoch sein
- Kopplung ist der Zusammenhang zwischen verschiedenen Klassem - sollte so gering wie möglich gehalten sein
- "once and only once" (Folie Teil 2 S.25) erreicht durch hohe Kohäsion
- "Einfacher Entwurf" (Folie Teil 2 S.29) erreicht durch geringe Kopplung

## Monolithisches System

![](https://gitlab.com/cediwelli/lernen/-/raw/main/monolithischessystem.png)

## 3-Schichten-Architektur (Folie Teil 2 S.27)

- externes Schema
	- Benutzeroberfläche
	- GUI
	- Kommunikation zwischen Benutzer und Systemen
konzeptuelles Schema
	Geschäftslogik
	Backend
internes Schema
	Datenbank
	
### Vorteile (zum monolithischem System)

- Kopplung wird minimiert
- Kohäsion wird maximiert
- leicht verständlicher/wartbarer/veränderbarer Code

## Information Hiding

- David Parnas, 1972

### Bedeutung

- Es werden beim Programmieren Konzepte, Informationen und Zugänge versteckt, bzw. für andere Programmierer, die z.B. nur auf Schnittstellen zugreifen, unzugänglich gemacht.
- z.B. die Funktionsweise von Funktionen muss dem Programmierer, der diese Funktion benutzt, nicht bekannt sein. Es ist nur wichtig zu wissen was die Funktion berechnen soll sowie was rein und was raus kommt

### Nutzen

- Es soll verhindert werden, dass Programmierer Daten verändern, auf die sie keinen Zugang haben sollten.
- z.B. eine Funktion, die Daten zu einer Liste hinzufügt und noch weitere Subroutinen ausführt: auf diese Liste soll der Programmier keinen Zugriff haben, um diese Subroutinen nicht (ausversehen) zu überspringen. 
- Am wichtigsten ist es, dass die Programmierer nicht erst verstehen müssen, wie die Funktion funktioniert, um sie benutzen zu können.

## Open-Closed Principle

### "Geöffnet für Erweiterungen, geschlossen für Modifikationen"

- schreibt vor, dass Objekte (Klassen, Module, Funktionen, etc) offen für Erweiterungen, aber geschlossen für Modifikationen sein sollen, was bedeutet, dass eine Entität ihr Verhalten ändern darf, ohne ihren Code zu ändern
- Damit ist vor allem gemeint, dass nicht wieder neu-kompiliert werden muss. Z.B., wenn man einen Dateipfad hardcoded. Man könnte den Dateipfad über eine Eingabe übergeben, sodass der Code nicht geändert werden muss.
- z.B. Composite Pattern (Folie Teil 2 S.98/99)

## Design Patterns 

- Idee: Entwurfserfahrungen verpacken, wiederholt nutzen.
- Ansatz:
	- Gewisse Probleme im Entwurf tauchen immer wieder auf
	- Erfahrene Entwickler finden ähnliche bewährte Lösungen
	- Idee: Problem-Lösungspaar wird als Muster beschrieben
	- Wenn das Problem nur einmal auftaucht, dann gleiche Lösungn einsetzen
- Vorteile:
	- Erfahrungen sind in Patterns "codiert", nicht im Hirn
	- Anwendung auch für nicht so erfahrene Entwerfer möglich
	- Nur das beste Muster muss man sich merken
- "Gang of Four"

## Observer Pattern (Folie Teil 2 S.89)

- Observer passiv, Subject aktiv
- Die Observer werden an Subjects attached
- Wenn sich der Zustand des Subjects ändert, werden die Update-Methoden der Observer, die attached wurden, aufgerufen
- Der Wert kann über die Update-Methode übergeben werden, >oder, es wird das Subject direkt in der Update-Methode übergeben, damit der Observer sich dessen Werte holen kann<

## Adapter Pattern (Folie Teil 2 S.90)

- Eingaben unterschiedlichen Formats auf gewünschtes Format anpassen
- Schnittstellen-Adapter

## Composite Pattern (Folie Teil 2 S.97)

- Objekte werden gruppiert
- Clients, die diese benutzen, sollen aber nicht unterscheiden müssen zwischen atomaren und zusammengesetzten Objekten
- Leaf: atomares Objekt, kann keine Kinder haben
- Composite: Zusammengesetztes Objekt, gibt Operationen rekusiv an Kinder weiter
- Leaf und Composite sind Implementationen des Component-Interface
- Beispiel: Grafischer Editor

## MVC Pattern (Model View Controller) (Folie Teil 2 S.109)

- 3-Schichten Architektur
- MVC regelt die Interaktionen innerhalb und zwischen der Präsentationsschicht und Geschäftslogikschicht
- siehe Folie für weitere Informationen

## 3-Ebenen Diagramm (Folie Teil 1 S.113)

- Summary Goals
- User Goals
- Subfunctions

## Use-Cases (Folie Teil 1 S.109-115)

### Einsatz

1. Akteure identifizieren	
	1. Wer benutzt das System? Wozu? -> Stakeholder/Profiltabelle
	2. Wer ist mit Ein- und Ausgaben konfrontieret? -> Akteure

2. Systemgrenzen festlegen
	- Akteure sind außen, Use-Cases beschreiben das Hin und Her

3. Wichtigste Use-Cases identifizieren
	- narrative, Beziehungen zwischen Use-Case und Actor herstellen (UML)

4. Zunehmend genauer beschreiben
	1. narrative (Geschichtchen)
	2. Kurzfassung -> informell/detailiert
		1. Erfolgsszenarien
		2. Fehlerszenarien
	3. Bei Use-Cases mit dem Kunden diskutieren
	4. Bei Bedarf grafischer Übersicht ergänzen

![](https://gitlab.com/cediwelli/lernen/-/raw/main/2022-02-08-110757_837x655_scrot.png)

### Wann ist man fertig?

-  Alle Hauptakteure wurden identifiziert
- Hauptakteure haben ihre Ziele genannt
- Jedes Ziel wird von mindestens einem Use-Case abgedeckt
- Sponsoren bestätigen (agree), damit Abnahme machen zu können
- Nutzer bestätigen, dass Systemverhalten zutreffend beschrieben#
- Sponsoren bestätigen, Use-Cases vollständig
- **man ist nie entgültig fertig**

### Use-Case-Diagramm

- Systemgrenzen nicht vergessen
- Include Pfeile in Flussrichtung mit gestrichelter Linie und offener Pfeilspitze.
- Extend Pfeile zeigen auf das, was sie erweitern (Bsp.: Quittung drucken erweitert Bestellung abschließen)
- stellt keinen sequenziellen Ablauf da
- Hauptebene: Use-Cases, die eine direkte Verbindung zum Akteur haben

![](https://gitlab.com/cediwelli/lernen/-/raw/main/UseCaseDiagram.png)

## Risikomanagement

- Was könnte schief gehen?
- Bsp.:
```
R1: Wenn ...
    Dann ...
``` 
![](https://gitlab.com/cediwelli/lernen/-/raw/main/Risikomanagement.png)

## Dokumentation als Kommunikationsmittel

### Sortiert nach Effektivität (aufsteigend)

1. Papier
2. Audiotape (Tonband)
3. E-mail
4. Videotape (Videoaufzeichnung)
5. Telefon
6. Videocall (Videounterhaltung)
7. Face-to-Face (von Angesicht zu Angesicht)
8. Face-to-Face mit Whiteboard

## Agil vs. Traditionell (Folie Teil 1 S.142)

|  | Traditionell | Agil |
|--|--|--|
|**Mitwirkung des Kunden** |unwahrscheinlich|kritischer Erfolgsfaktor|
|**Etwas Nützliches wird geliefert**|erst nach einiger (längerer) Zeit|mindestens alle sechs Wochen|
|**Das Richtige entwickeln durch**|langes Spezifizieren/Vorausdenken|Kern entwickeln, zeigen, verbessern|
|**Nötige Disziplin**|formal, wenig|informell, viel|
|**Änderungen**|erzeugen Widerstand|werden erwartet und toleriert|
|**Kommunikation**|über Dokumente|zwischen Menschen|
|**Vorsorge für Änderungen**|durch Versuch der Vorausplanung|durch "flexibel Bleiben"|

## Testen (Folie Teil 2 S.162)

### Definition

- Ausführen eines Programms mit dem Ziel, Fehler zu finden

### Testen ist nicht

- Wunsch zu zeigen, dass das Program in Ordnung ist
- Herumprobieren-ändern-wieder Probieren
- ein unmittelbar konstruktiver Verbesserungsschritt

### Testfall

- konkret
- spezifisch
- leicht anwendbar
- objektiv beurteilbar
- testbar

- Prüfling: Zu prüfende Software
- Autor: Verfasser des Prüflings

![](https://gitlab.com/cediwelli/lernen/-/raw/main/2022-02-08-110924_892x251_scrot.png)

### Ein Test besteht aus

- Eingabe
- Soll-Ausgabe
- Setup

## COCOMO (Folie Teil 3 S.7)

- Barry Boehm
- Aufwandsschätzung

![](https://gitlab.com/cediwelli/lernen/-/raw/main/Cocomo.png)

## Burndown Chard (Folie Teil 3 S.15)

- verbleibende Storypoints über eine Iteration 

## Sequenzdiagramm

![](https://gitlab.com/cediwelli/lernen/-/raw/main/Sequenzdiagramm.png)
## Modelltheorie

- Was ist das Original
- Für wen ist das Modell?
- Für welchen Zweck, für welche Operation dient das Modell?
- Welche Eigenschaften sind für das Modell relevant?

## Klassendiagramm

![](https://images-ext-1.discordapp.net/external/XnKgfcfwE_b90o6zY1NcYZJz-ZuXewgok0MyFmi1fHQ/https/gitlab.com/cediwelli/lernen/-/raw/main/Klassendiagramm.png?width=724&height=659)
## Projektplanung

- Die 6 W
	- Was getan wird?
	- Warum?
	- Für wieviel Geld?
	- Von Wem?
	- Wann?
	- Womit? (Entwicklungs- und Management-Hilfsmittel und -Techniken)
