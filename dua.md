# Datenstrukturen und Algorithmen

## AVL-Tree Visualization

https://www.cs.usfca.edu/~galles/visualization/AVLtree.html

## B-Tree Visualization

https://www.cs.usfca.edu/~galles/visualization/BTree.html

## MST & MSB

- Minimum Spanning Tree (Minimaler Spannbaum)

## Dijkstra-Algorithmus

https://algorithms.discrete.ma.tum.de/graph-algorithms/spp-dijkstra/index_de.html

## Prim's Algorithmus

https://algorithms.discrete.ma.tum.de/graph-algorithms/mst-prim/index_en.html

## TSP

- Traveling-Salesman-Program (Fahrender Händler Problem)
