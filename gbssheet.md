
## Betriebssystem

| Aussage | Antwort | Zusatz |
|--|--|--|
| Das Betriebssystem erweitert konzeptionell den Behehlssatz des Prozessors | Richtig | Wenn damit gemeint ist, dass das Betriebssystem auf gewisse E2-Instuktionen reagiert und bestimmte Handler ausführt, dann ja |
| Multiplexing und Isolation sind Teil der Kernaufgaben eines Betirebssystems | Richtig | Steht in der Vorlesung, lol |
| Durch Ausführen eines Programms als Administrator gelangt man in den priviligierten Modus | Falsch | Der priviligierte Modus (Systemmodus) ist ausschließlich dem Betriebssystem vorbehalten (Im Systemmodus werden E2-Instruktionen direkt ausgeführt) |
| Mikrokernbetriebssysteme bieten nur einen stark reduzierten Instruktionsumfang | Richtig | Mikrokernbetriebssysteme erfüllen nur grundlegende Funktionen |
| Die Unterstützung des Mehrprogrammbetriebs durch das Betriebssystem erfordert mehr als einen Prozessor | Falsch | Dafür gibt es Scheduler |
| Ein Unix-Prozess ist ein virtueller Computer | Richtig | Prozesse haben ihren eigenen virtuellen Prozessor, Adressraum und virtualisieren somit einen Computer |

## fork

| Frage | Antwort | Zusatz |
|--|--|--|
| Wird im Fehlerfall bei `fork()` im Kind-Prozess -1 zurückgegeben? | Nein. | Im Fehlerfall wird kein Kind erzeugt. |
| Wird dem Elternprozess die Prozess-ID des Kindes zurückgeliefert? | Ja | lol |
| Bekommt der Kind-Prozess die Prozess-ID des Eltern-Prozesses? | Nein |Kind-Prozess bekommt 0|
| Ist der Rückgabeprozess in jedem Prozess (Kind & Eltern) jeweils die eigene Prozess-ID? | Nein | Das Kind bekommt 0 und das Elternteil die ID vom Kind |

## Nebenläufigkeit

Es kann zu Nebenläufigkeiten kommen durch...

| Frage | Antwort | Zusatz |
|--|--|--|
| ...dynamische Bibliotheken? | Nein | Dynamische Bibliotheken erweitern das Programm |
| ...interrupts? | Ja | Durch Interrupts werden BS-Funktionalitäten erfordert, wodurch es aktiv wird |
| ...langfristiges Scheduling? | Nein? |  |
| ...traps? | Nein | werden durch den Code ausgelöst, sind also synchron |
|  |  |  |

## Einprozessorbetriebssystem

| Aussage | Antwort | Zusatz |
|--|--|--|
| Ein Prozess im Zustand "blockiert" muss warten, bis der laufende Prozess den Prozessor abgibt, und kann dann in den Zustand "laufend" überführt werden | Falsch | Muss erst in bereit überführt werden. |
| Es befindet sich zu einem Zeitpunkt maximal ein Prozess im Zustand laufend | Richtig | Nur ein Prozess kann vom Prozessor bearbeitet werden |
| In den Zustand "blockiert" gelangen Prozesse nur aus dem Zustand "bereit" | Falsch | Ein Prozess kann nur blockieren, wenn er auf Betriebsmittel wartet. Da er in "bereit" nicht die Möglichkeit hat, auf Betriebsmittel zu warten, kann er auch nicht blockieren |
| Ist zu einem Zeitpunkt kein Prozess im Zustand "bereit", so ist auch kein Prozess im Zustand "laufend" | falsch | ergibt kein Sinn |

## Prozesszustände

| Aussagen | Antwort | Zusatz |
|--|--|--|
| Der Planer (Scheduler) kann einen Prozess in den Zustand blockiert überführen, indem er einen anderen Prozess einlastet | Falsch | Er muss suspendiert werden, und kommt dann in "angehalten bereit" |
| In einem 4-Kernsystem können sich maximal 4 Prozesse gleichzeitig im Zustand "bereit" befinden | falsch | Es kann so zu viele Prozesse warten, wie Platz dafür ist |
| Blockierte Prozesse werden in den Zustand bereit überführt, wenn die benötigten Betriebsmittel zu Verfügung stehen | Richtig | siehe Zustandsübergangsdiagramm |
| In einem 4-Kernsystem gibt es stehts 4 laufende Prozess | Falsch | Es kann auch weniger als 4 laufende Prozesse geben |

## Betriebsmittel

| Aussage | Antwort | Zusatz |
|--|--|--|
| Hardwarebetriebsmittel können Prozessen immer nur exklusiv zugeteilt werden | Falsch | z.B. Monitor |
| Speicher verliert bei Stromausfällen seinen Inhalt. Er gilt deshalb als transientes Betriebsmittel | Falsch | Der Speicher ist permanent. Transient wären konsummierbare Betriebsmittel wie Signale oder Nachrichten |
| Konsummierbare Betriebsmittel werden zur Laufzeit generiert und verbraucht | Richtig? |  |
| Der Zugriff auf Betriebsmittel erfordert immer synchronisation | Richtig | Steht auf Folie, lmao |

## Threads

| Aussage | Antwort | Zusatz |
|--|--|--|
| Bei User-Threads (Fasern) ist die Scheduling-Strategie nicht durch das Betriebssystem vorgegeben | Richtig | Weil nur Kernel-Threads vom Betriebssystem eingeplant werden. User-Threads sind eigenimplementationen. Siehe JVM |
| Kernel-Threads können Multiprozessoren ausnutzen | Richtig |  |
| Die Umschaltung zwischen User-Threads ist eine priviligierte Operation und muss deshalb im Systemkern erfolgen | Falsch | User-Threads haben mit dem Systemkern nichts zu tun. |
| Für jeden Kernel-Thread verwaltet das Betriebssystem einen eigenen geschützten Adressraum | Falsch | Threads haben einen übergreifenden Adressraum innerhalb ihres Prozesses |

## Adressraumschutz

| Aussage | Antwort | Zusatz |
|--|--|--|
| Der Adressraumbelegungsplan (memory map) beschreibt für ein Programm, auf welche Speicheradressen es zugreift. | Falsch | Die Memory Map ist der Speicherbelegnungsplan für das Betriebssystem |
| Ein Nachteil der wortorientierten Segmentierung sind Speicherverluste durch internen Verschnitt | ? |  |
| Mit dem Einsatz von Segmentierung ist es möglich diesselbe logische Addresse in unterschiedlichen logischen Adressräumen auf verschiedene physikalische Adressen im realen Adressraum abzubilden | ? | Cedric: Weiß nicht ob das durch Segmentierung passiert |
| In einem segmentierten Adressraum kann zur Laufzeit kein weiterer Speicher mehr dynamisch nachgefordert werden | falsch? |  |

## Dateideskriptoren

| Aussage | Antwort | Zusatz |
|--|--|--|
| Ein Dateideskriptor ist eine Integerzahl, die über gemeinsamen Speicher an einen anderen Prozess übergeben werden kann und von letzterem zum Zugriff auf eine geöffnete Datei verwendet werden kann | Falsch | Dateideskriptoren sind Prozessspezifisch, und müssen speziell geklont werden |
| Bei mehrfachen Öffnen ein- und derselben Datei erhält der Prozess jeweils die gleiche Integerzahl als Dateideskriptor zurück | falsch | Kriegrich hat es getestet |
| Beim Aufruf von fork() werden zuvor geöffnete Dateideskriptoren in den Kindprozess vererbt | Richtig | Ja |
| Der Aufruf newfd = dup(fd) erzeugt eine Kopie, der dem Dateideskriptor fd zugrundeliegenden Datei; newfd enthält einen Dateideskriptor auf die neu erzeugte Datei überreicht | Falsch | dup erzeugt einen neuen Dateideskriptor, der auf die gleiche Datei zeigt, wie fd |
| Vor dem Beenden eines Prozesses muss man alle geöffneten Dateien mit close() schließen, da sonst die dazugehörigen Ressourcen verlorengehen | Falsch | Alle zugehörigen Ressourcen werden automatisch Freigegeben, nachdem der Prozess beendet wurde |

## Signale

| Frage | Antwort | Zusatz |
|--|--|--|
| Alle mit Kill() versendeten Signale führen zum Beenden des Prozesses | Falsch | Kill kann benutzt werden, um Signale an andere Prozesse zu senden über Signale |
| Durch Signale können Nebenläufigkeitsprobleme in Grundsätzlich nicht-parallelen Programmen entstehen | Richtig | Signalhandler können theoretisch Zustände oder Variablen verändern, und dies kann durch externe Quellen entstehen |
| Es gibt synchrone und asynchrone Signale | Richtig | synchrone Signale werden durch den eigenen Prozess ausgelöst, und sind z.B. traps. Asynchrone Signale sind externe Ereignisse|
| Signale sind das Unix-pendant zu Traps/Interrupts auf Hardware-Ebene | Richtig? |  |

## Traps und Interrupts

| Aussage | Antwort | Zusatz |
|--|--|--|
| Der Zeitgeber (Systemuhr) unterbricht die Programmbearbeitung in regelmäßigen Abständen. Die genaue Stelle der Unterbrechung ist damit vorhersagbar, somit sind solche Unterbrechungen der Kategorie Trap einzuordnen | ? | T_T |
| Normale Rechenoperationen können zu einem Trap führen | Richtig | Division durch 0 |
| Systemaufrufe sind jederzeit möglich, und deshalb in der Kategorie Interrupts einzuordnen | Falsch | Da Systemcalls nicht asynchron von der Hardware erzeugt werden. |
| Ein Trap führt zwingend zum Abbruch des Prozesses, da er einen schwerwiegenden Fehler dastellt | Falsch | Es wird versucht, den Fehler auf der E2-Ebene zu beheben, dann wird die Instruktion wiederholt oder abgebrochen (beendet) |
| Ein Zugriff auf eine Speicheradresse kann zu einem Trap führen | Richtig | Falls ausserhalb des eigenen Adressraums Zugriff angefordert wird |

| Frage | Antwort | Zusatz |
|--|--|--|
|  |  |  |
|  |  |  |

