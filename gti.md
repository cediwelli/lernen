# Grundlagen der theoretischen Informatik

## Grammatik

- G = (V, Sigma, P, S)
- V = Variablen {S, A, B}
- Sigma = Terminalzeichen {a, b}
- P = Produktion (Übergänge)
- S = Startsymbol

### Grammatikbedingungen

- Typ1 (Kontextsensitiv): Für alle u -> v gilt |u| <= |v|
- Typ2 (Kontextfrei): u ist einzelne Variable
- Typ3 (Regulär): v besteht aus einzelnen Terminalzeichen oder Terminalzeichen gefolgt von Variable

## Automaten

### Endlicher Automat

- Tupelschreibweise: M = (Z, Sigma, Delta, z_0, E)
- Z = Zustände (endlich)
- Sigma = Eingabealphabet (Z geschnitten Sigma = leere Menge)
- Delta = Überführungsfunktion
- z0 = Startzustand
- E = Endzustände

### Pumping Lemma

- Vorlage (reguläre Sprache): 
Angenommen L sei eine reguläre Sprache. Wenn L regulär ist, dann existiert beliebiges n Element aus natürliche Zahlen, sodass für alle Wörter x aus L mit |x| >= n eine Zerlegung x = uvw existiert mit:
	1. |v| >= 1
	2. |uv| <= n
	3. Alle i >= 0 : uv'w Element aus L

Wir wählen x = ?. |x| >= n. 

Antwortsatz: Da x' nach abpumpen/aufpumpen nicht aus L ist, ist dies ein Widerspruch zum Pumping Lemma, und deshalb ist L nicht regulär.

- Vorlage (kontextfreie Sprache):
- Angenommen, L sei ein kontextfreie Sprache. Dann gibt es eine Zahl n, sodass sich alle Wörter z aus L mit |z| >= n, n Element aus N mit n beliebig, zerlegen lassen in z = uvwxy, sodass folgende Eigenschaften erfüllt sind:
	1. |vx| >= 1
	2. |vwx| <= n
	3. Für alle i >= 0 gilt: uv^i^wx^i^y Element aus L

Wir wählen z = ?, |z| >= n.

Antwortsatz: Da z' nach abpumpen/aufpumpen nicht aus L ist, ist dies ein Widerspruch zum Pumping Lemma, und deshalb ist L nicht regulär.

### Kellerautomat

- Akzeptiert, wenn Eingabe komplett gelesem wurde, und Endzustand erreicht.
- Wenn der Stack leer wird, und dann gelesen wird, stürzt der Automat ab
- Wenn Stack leer wird, aber ein Endzustand erreicht wird, und die Eingabe komplett gelesen wurde, wird trotzdem akzeptiert
- M = (Z, Sigma, Gamma, Delta, z_0, #, E)
- Z = Zustandsmenge
- Sigma = Eingabealphabet
- Gamma = Arbeitsalphabet (mit #)
- z_0 = Startzustand
- \# = erste, unterste Kellersymbol
- E = Endzustandsmenge

Delta Bsp.: z_0a# -> z_0ACB
Erklärung: Wird befinden uns in z_0, lesen ein a, und haben ein # oben auf dem Keller, wir gehen über zu Zustand z_0 und wir überschreiben das oberste Kellersymbol mit B und pushen C und A (in dieser Reihenfolge).

```
pop
push B
push C
push A
```

## Reguläre Grammatik zu endlicher Automat

- Wenn Epsilon in L(M), dann A_z_0 -> Epsilon
- Falls d(z_x, a) = z_y, dann A_z_x -> aA_z_y
- V = {A_z | z aus Z}
- S = A_z_0

![](https://gitlab.com/cediwelli/lernen/-/raw/main/regulaeregrammatikDEA.png)

## Turing-Maschine

- M = (Z, Sigma, Gamma, Delta, z_0, Blank, E)
- Z = Zustände
- Sigma = Eingabealphabet
- Gamma = Arbeitsalphabet (mit Blank)
- z_0 = Startzustand
- Blank = leeres Symbol
- E = Endzustände

Turing Machine akzeptiert, sobald ein Endzustand betreten wird.
Bsp.: z_0a -> z_1AR
Erklärung = Wir sind in Zustand z_0 und der Schreib-/Lesekopf zeigt auf das Zeichen a. Wir gehen über in Zustand z_1, schreiben ein A an die gleiche Stelle und machen eine Kopfbewegung nach rechts.

- Satz 66: Jede Mehrband-TM kann zu einer Einband-TM umgewandelt werden.

## Berechenbarkeit

### Was ist berechenbar?

- Eine Funktion heißt berechenbar, falls ein Rechenverfahren, bzw ein Algorithmus existiert, der die Funktion berechnet. Das heißt, gestartet mit einer Eingabe, hält der Algorithmus nach endlich vielen Schritten mit einer Ausgabe.
- Die Funktion muss nicht total sein, dass heißt, für gewisse Eingaben darf die Funktion undefiniert sein. In diesem Fall soll der Algorithmus nicht stoppen (Endlosschleife)
- Bsp: 
![](https://gitlab.com/cediwelli/lernen/-/raw/main/ab1.png)
- Ja, weil es nur endlich viele Studierende gibt, die die Klausur schreiben werden, von denen können wir das Klausurergebnis berechnen, auch wenn wir nicht wissen wie.

## LOOP/WHILE-Programme

- K-stellige Funktionen bedeutet K-Eingangsvariablen.

## Entscheidbarkeit

![](https://gitlab.com/cediwelli/lernen/-/raw/main/entscheidbarkeit.png)
- Eine Sprache ist entscheidbar, wenn es einen Algorithmus gibt, der entscheidet, ob ein Wort in der Sprache ist oder nicht

## Rekursiv-aufzählbar

- Definition: Eine Sprache heißt rekusiv aufzählbar, falls die Sprache leer ist, oder falls es eine totale, berechenbare Funktion gibt, sodass A = {f(0), f(1), f(2)}.
- Wir sagen: f zählt A auf.
- 
- Satz 85: Eine Sprache ist rekursiv-aufzählbar genau dann wenn sie semi-entscheidbar ist

## Kurzfragen

Q: Für ein bestimmtes Alphabet gibt endlich viele Sprachen.
A: Falsch

Q: Wenn L Kontextfrei ist, dann gibt es eine Turing Maschine M, sodass L(M) = L gilt.
A: RIchtig

Q: Wenn L kontextsensitiv ist, dann gibt es eine WHILE-Programm, welches die charakteristische Funktion von L berechnet.
A: Richtig

Q: Die Sprache (w | Mw berechnet die Funktion f(x) = x.) ist entscheidbar.
A: Falsch


