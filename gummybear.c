sem_t gummy_bear_mass_mutex;
sem_t green_robot_mutex;
int collecting_green_robots;

void init(char * path) {
	sem_init(&gummy_bear_mass_mutex, 0, 1);
	sem_init(&green_robot_mutex, 0, 1);
	collecting_green_robots = 0;
}

void green_robot() {
	while (1) {
		sem_wait(&green_robot_mutex);
		collecting_green_robots++;
		if (collecting_green_robots == 1) {
			sem_wait(&gummy_bear_mass_mutex);
		}
		sem_post(&green_robot_mutex);
		green_robot_collect();
		sem_wait(&green_robot_mutex);
		collecting_green_robots--;
		if (collecting_green_robots == 0) {
			sem_post(&gummy_bear_mass_mutex);
		}
		sem_post(&green_robot_mutex);
		green_robot_process();
	}
}

void red_robot(void) {
	while (1) {
		red_robot_process();
		sem_wait(&gummy_bear_mass_mutex);
		red_robot_collect;
		sem_post(&gummy_bear_mass_mutex);
	}
}
