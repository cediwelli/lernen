

## Ebenenmodell

![](https://images-ext-1.discordapp.net/external/rXX5hDc8AKv0LDLTVk0I0sqaI76582hKeu5QQq5RRZ4/https/gitlab.com/cediwelli/lernen/-/raw/main/Ebenenmodell.png)

## Schutz

- Mit welchen **Schutzmechanismen** werden Hardwareressourcen verwendet? 
	- Hardware wird virtualisiert und den Bewerbern als Betriebsmittel zur Verfügung gestellt (Multiplexing)
	- Prozesse erhalten einen eigenen virtuellen Adressraum

## Betriebssystemmodus

1. Benutzermodus: Bedeutet, dass manche E2-Instruktionen nicht zum Prozessor durchgereicht werden, sondern vom Betriebssystem interpretiert werden müssen
2. Systemmodus: Ist ein priviligierter Modus, den nur das Betriebssystem hat. Hier werden alle E2 Instruktionen zum Prozessor durchgereicht

## Kernaufgaben

- Multiplexen von Ressourcen
- Isolation von Prozessen und Ressourcen
- Verwaltung der virtuellen Hardware Ressourcen
	- Prozessverwaltung
	- Speicherverwaltung
- Dateisystem
- Gerätesteuerung
- Netzwerkzugriff

![](https://images-ext-2.discordapp.net/external/akhhkyxWwRhkrTS3BFcTEeSgVeM8Wr135yXjQisv7HA/https/gitlab.com/cediwelli/lernen/-/raw/main/BS.png)

## Dateisystem

- Kontext: Definiert Flacher Namensraum Verzeichnis
- Dateibaum hat hierachische Struktur
- Nicht die Objekte selbst, sondern die Verknüpfungen sind benannt
- Dateibaum ist azyklischer Graph
- Der tatsächliche Namensraum ist flach (durchnummerierte Dateien)

![](https://images-ext-2.discordapp.net/external/Zhx3KwJ731RABfIwY7ZKDXIcq0ky8vTAqw2D_GaVDN8/https/gitlab.com/cediwelli/lernen/-/raw/main/Blockbyte.png)
![](https://images-ext-1.discordapp.net/external/jEMz1g3Qk3emrHZshcTkiO1WnW1Su41rkZmKiV734eM/https/gitlab.com/cediwelli/lernen/-/raw/main/Datenblock.png)
![](https://images-ext-1.discordapp.net/external/dj9QbvROrTKi1QsdkOiwt2cxpXCTBoMJmrjNStI554Y/https/gitlab.com/cediwelli/lernen/-/raw/main/Virtuelle%2520Hierachie.png)

### Verzeichnis

- Ein Katalog von Verknüpfungen
	- Verweis auf sich selbst 
	- Verweis auf übergeordnetes Verzeichnis

### Hardlinks

- Sind das gleiche wie ein normaler Verweis auf ein Dateiobjekt
- Ein Dateiobjekt kann mehrere Hardlinks
- Hardlinks auf Verzeichnisse sind nicht möglich

### Softlinks

- Sind eigene Dateien, die auf einen Pfad verweisen
	- Der Pfad ist in der Datei gespeichert

### Virtuelles Filesystem (VFS)
 
 - Bildet echte Dateisysteme von Festplatten auf den azyklischen Dateigraphen ab

![](https://images-ext-2.discordapp.net/external/kZd2186w6UYPWK4Wu0l0ILEggRGAp4pJgvDaDkL6GlA/https/gitlab.com/cediwelli/lernen/-/raw/main/VFS.png)
 - 
### Dateiattribute

- Indexknotenattribute (Metadaten)
	- Dateityp (Reguläre Datei, Verzeichnisdatei, Softlink, Gerät, Pipe, Socket)
	- Anzahl der Verzeichnisverweise (Linkcounter) (wird Linkcounter 0, wird die Datei gelöscht)
	- Größe der Datei
	- Eigentümer der Datei (UserID)
	- Gruppenzugehörigkeit (GroupID)
	- Rechte (Für Eigentümer, Gruppe und Welt)
	- Zeitstempel (letztes Zugriff, letzte Änderung, letzte Attributsänderung)
- Indexknotennumner ist eindeutig innerhalb des Dateisystems

### Betriebsmittel

![](https://images-ext-2.discordapp.net/external/Fim8kHxVDAa21L12wrA3aPZcpS7VFuuIbahOD7_FTm8/https/gitlab.com/cediwelli/lernen/-/raw/main/Betriebsmittel.png)
## ELF

![](https://images-ext-2.discordapp.net/external/QeYChSdzKEybixq1LAiYtrFUJVkZuW0qtHW74PVWrAA/https/gitlab.com/cediwelli/lernen/-/raw/main/ELF.png)
## Prozessplanung

![](https://gitlab.com/cediwelli/lernen/-/raw/main/prozesszustande.png)
![](https://gitlab.com/cediwelli/lernen/-/raw/main/Prozesseinplanung.png)

## Aufgaben

1. Q: Beschreiben sie den Begriffe **Prozess**, **Programm** und **Datei** im Kontext von Betriebssystemen. Setzen sie diese in Verbindung zueinander.

A: 
Prozess:
- Prozesse sind schwergewichtige Ausführungskontexte.
- eigener Adressraum, eigene Dateideskriptortabelle, Befähigungen, Signale
- Segmente für Text (Code), Daten (Data), zwei Stapel (User/Kernel Stacks)
- CPU Zustand der aktuellen Ausführung (bei verdrängten Prozessen)
- Ein Prozess multiplext und virtualisiert einen vollständigen Computer

Programm:
- Ein Programm ist eine Anweisungsfolge, und spezifiziert die Anweisungen für einen Prozessor

Datei:
- Ein Bestand sachlich zusammenhängender, unstrukturierter Daten, welche in der Regel dauerhaft im Hintergrundspeicher vorrätig sind.

Beziehung:
- In einem Prozess wird ein Programm ausgeführt, dieser Prozess kann mehrere Dateideskriptoren besitzen, die auf echte Dateien zeigen und das Programm ist in der Regel selbst in einer Datei gespeichert

2. Im Mehrprogrammbetrieb ist es nicht möglich, dass alle Programme beim Laden an die Speicheradresse, die beim Linken angenommen wurde, platziert werden können. Wie geht das Betriebssystem damit um?
- A1: PIC (Positionsunabhängiger Code) 
	- Zugriffe auf Code und Daten erfolgt immer Adressregister-indirekt
	- Bei Codezugriffen soweit möglich sowie möglich über den PC (Program Count)
	- Bei Datenzugriffen über ein festgelegtes Basisregister und Offset
	- Basisregister wird beim Laden auf Ladeadress der Sektion gelegt
	- Erfordert Unterstützung durch Compiler/Linker 
- A2:
	- Beim Ladevorgang den Code anpassen (patchen)
	- Anzupassen ist jeder Verweis (Zeiger) auf ein Symbol
	- Compiler und Linker exportieren Tabelle auf Symbolreferenzen
	- Jede Symbolreferenz wird entsprechend der Ladereferenz angepasst
A3:
	 Hardware gestützt durch Segmentierung -> Logischer Adressraum

3. Bei Schedulingverfahren wird in Offline und Online Scheduling unterschieden. Nennen sie den entscheidenen Unterschied und einen Vorteil

Online Scheduling ist dynamisch, und es werden während der Ausführung die Prozesse eingeplant. Wohingegen Offline Scheduling statisch ist, die Prozesse werden vor der Ausführung eingeplant. Ein Vorteil von Online Scheduling ist, dass die Laufzeiten der Programme nicht im Vorraus gewusst werden müssen. Ein Vorteil von Offline Scheduling ist, dass ein sehr hoher Durchsatz erzielt werden kann, und er sehr schnell ist.

